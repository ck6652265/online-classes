import { Col, Input, message, Row, Tag } from 'antd'
import axios from 'axios'
import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import EditModal from './EditModal'
import Header from './Header'
import './index.css'
export default class Edit extends Component {
  constructor (props) {
    super(props)
    this.state = {
      title:
        (props &&
          props.location &&
          props.location.state &&
          props.location.state.title) ||
        '',
      id:
        (props &&
          props.location &&
          props.location.state &&
          props.location.state.id) ||
        '',
      userType:
        (props &&
          props.location &&
          props.location.state &&
          props.location.state.userType) ||
        '',
      uri: 'http://localhost:4000',
      isUpdated: false,
      isOpen: false,
      filterWords: [],
      wordsData: null,
      selectedWordName: ''
    }
  }
  componentDidMount = () => {
    this.get()
  }
  formSubmit = e => {
    e.preventDefault()
    const { title, description, id, uri } = this.state

    if (title && description) {
      let payload = {
        title: title,
        description: description
      }
      axios
        .put(uri.concat(`/api/v1/lesson/${id}`), payload)
        .then(res => {
          let data = res.data
          if (data.success) {
            message.success(data.msg)
            this.setState({ isUpdated: true })
          }
        })
        .catch(err => {
          message.error('DB error.')
        })
    }
  }
  get = () => {
    const { uri, description, id } = this.state
    let new_uri = uri.concat(`/api/v1/words/lesson/${id}`)
    axios
      .get(new_uri)
      .then(res => {
        let data = res.data
        if (data.success) {
          this.setState({
            filterWords: data.data
          })
        }
      })
      .catch(err => {})
  }

  changeField = e => {
    this.setState({ [e.target.name]: e.target.value })
  }
  click = (obj, wordName) => {
    if (obj) {
      this.setState({ wordsData: obj })
    }
    this.setState({ isOpen: !this.state.isOpen })
  }
  handleModal = e => {
    this.setState({ isOpen: !this.state.isOpen })
  }
  changeColor = c => {
    const { wordsData } = this.state
    this.setState({ wordsData: { ...wordsData, color: c } })
  }
  changeHandler = e => {
    const { wordsData } = this.state
    this.setState({
      wordsData: { ...wordsData, [e.target.name]: e.target.value }
    })
  }
  submitHandler = data => {
    let lessonId = this.state.id
    let id = data.id || null
    let payload = {
      lessonId: lessonId,
      color: data.color,
      desc: data.description,
      image: data.image || '',
      video_url: data.video_url || ''
    }
    if (id) {
      // update
      axios
        .put(`http://localhost:4000/api/v1/words/${id}`, payload)
        .then(res => {
          let data = res.data
          if (data.success) {
            message.success(data.msg)
            this.get()
          }
        })
        .catch(err => {
          message.error('DB error.')
        })
    }
    this.handleModal()
  }
  render () {
    let {
      title,
      id,
      isOpen,
      userType,
      filterWords,
      wordsData,
      selectedWordName
    } = this.state
    if (!sessionStorage.getItem('userData')) {
      return <Redirect to={'/login'} />
    }
    if (!title || !id) {
      return <Redirect to={'/dashboard'} />
    }

    return (
      <div className='container'>
        <Header />
        <h2>Update Lesson</h2>
        <form
          className='container'
          style={{ width: '50%' }}
          onSubmit={this.formSubmit}
        >
          <Row>
            <Col span={4}>
              <label>
                <strong>Title:</strong>
              </label>
            </Col>
            <Col span={16}>
              <Input name='title' required value={title || ''} disabled />
            </Col>
          </Row>
          <Row>
            <Col span={4}>
              <label>
                <strong>Description:</strong>
              </label>
            </Col>
            <Col span={16}>
              <div className='desc'>
                {filterWords.map(item => {
                  const found =
                    filterWords.length > 0 &&
                    filterWords.find(f => f.name === item && f.lesson_id === id)
                  return (
                    <Tag
                      key={item.id}
                      color={item.color}
                      onClick={e => this.click(item)}
                    >
                      {item.name}
                    </Tag>
                  )
                })}
              </div>
            </Col>
          </Row>
          {userType === 'teacher' ? (
            <button type='submit' style={{ marginTop: '10px' }}>
              Update
            </button>
          ) : (
            ''
          )}
        </form>
        <EditModal
          isOpen={isOpen}
          handleModal={this.handleModal}
          data={wordsData}
          changeColor={this.changeColor}
          changeHandler={this.changeHandler}
          submitHandler={this.submitHandler}
          selectedWordName={selectedWordName}
        />
      </div>
    )
  }
}
