import { Alert, Button, Table } from 'antd'
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import './index.css'

const getDate = str => {
  let d = new Date(str).toString()
  let arr = d.split(' ')
  let dt = arr[2] + ' ' + arr[1] + ' ' + arr[3]
  return dt
}
const Lesson = props => {
  const { list } = props
  const [isOpen, setIsOpen] = useState(false)
  const [isUpdated, setIsUpdated] = useState(false)
  const [editData, setEditData] = useState(null)

  const handleModal = record => {
    setIsOpen(!isOpen)
    setEditData(record)
  }
  const changeData = () => {}
  const col = [
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title'
    },
    {
      title: 'Added By',
      dataIndex: 'added_by_name',
      key: 'added_by_name'
    },
    {
      title: 'Added On',
      dataIndex: 'created_at',
      key: 'created_at',
      render: (text, record) => getDate(text)
    },
    {
      title: 'View',
      dataIndex: 'link',
      key: 'link',
      render: (text, r) => (
        <Link
          key={r.id}
          to={{
            pathname: `/edit`,
            state: {
              id: r.id,
              title: r.title,
              userType: r.userType
            }
          }}
        >
          <Button type='primary' size='small'>
            View
          </Button>
        </Link>
      )
    }
  ]
  return (
    <div>
      {list.length ? (
        <>
          <Table dataSource={list} columns={col} />
        </>
      ) : (
        <div className='container' style={{ width: '500px' }}>
          <Alert message='No lessons' type='info' showIcon />
        </div>
      )}
    </div>
  )
}

export default Lesson
