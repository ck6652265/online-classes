import { message, Button } from 'antd'
import axios from 'axios'
import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import Header from './Header'
import './index.css'
import Lesson from './Lesson'
import User from './User'

export default class Dashboard extends Component {
  constructor () {
    super()
    this.state = {
      list: [],
      userid: '',
      name: '',
      userType: '',
      uri: 'http://localhost:4000'
    }
  }
  get = (uri, userType, userid) => {
    let new_uri = uri.concat('/api/v1/lesson')
    axios
      .get(new_uri)
      .then(res => {
        let data = res.data
        if (data.success) {
          this.setState({
            list: data.data
          })
        }
      })
      .catch(err => {
        message.error('DB error.')
      })
  }

  addLesson = () => {
    return (
      <a href='/add'>
        <Button type='primary' className='add-btn'>
          Add Lesson
        </Button>
      </a>
    )
  }
  componentDidMount = () => {
    const { uri } = this.state
    const userData = JSON.parse(sessionStorage.getItem('userData'))
    if (userData) {
      this.setState({
        userid: userData.id,
        name: userData.name,
        userType: userData.userType
      })
      this.get(uri, userData.userType, userData.id)
    }
  }

  render () {
    let { list, name, userType } = this.state
    if (!sessionStorage.getItem('userData')) {
      return <Redirect to={'/login'} />
    }
    return (
      <div className='container'>
        <Header />
        <User user={{ name, userType }} />
        <Lesson list={list} />
      </div>
    )
  }
}
