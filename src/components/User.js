import { Button, Card } from 'antd'
import React from 'react'
import './index.css'

const User = props => {
  const { user } = props

  return (
    <div>
      <Card title='Welcome User' type='inner' className='user-details'>
        <div className='user-details'>
          <label>Name:&nbsp;</label>
          <label>{user.name}</label>
        </div>
        <div className='user-details'>
          <label>User Type:&nbsp;</label>
          <label>{user.userType}</label>
        </div>
        <div>
          {user.userType === 'teacher' ? (
            <a href='/add'>
              <Button type='primary' className='add-btn'>
                Add Lesson
              </Button>
            </a>
          ) : (
            ''
          )}
        </div>
      </Card>
    </div>
  )
}

export default User
