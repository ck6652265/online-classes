import { Input, message, Select } from 'antd'
import axios from 'axios'
import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import './index.css'
const { Option } = Select
export class SignUp extends Component {
  constructor () {
    super()
    this.state = {
      uri:
        process.env.NODE_ENV === 'development' ? 'http://localhost:4000' : '',
      isRedirectReqd: false,
      name: '',
      email: '',
      password: '',
      isLoading: false,
      userType: ''
    }
    this.changeHandler = this.changeHandler.bind(this)
    this.formSubmit = this.formSubmit.bind(this)
  }

  changeHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  selectHandler = val => {
    this.setState({
      userType: val
    })
  }

  formSubmit = e => {
    e.preventDefault()
    const { uri, email, password, name, userType } = this.state
    if (!email || !password || !name || !userType) {
      message.error('Please fill/select all the fields.')
      return
    }
    let obj = {
      email: email,
      password: password,
      name: name,
      userType: userType
    }
    // call signup api
    axios
      .post(uri.concat(`/api/v1/user`), obj)
      .then(res => {
        let data = res.data
        if (data.success) {
          message.success(data.msg)
          this.setState({ email: '', password: '', name: '' })
        }
      })
      .catch(err => {
        message.error(`Error: ${err.response.data.msg}`)
      })
  }

  render () {
    if (sessionStorage.getItem('userData')) {
      return <Redirect to={'/dashboard'} />
    }

    const { isLoading, email, name, password } = this.state
    return (
      <div className='container'>
        <h2>Sign Up</h2>
        <form onSubmit={this.formSubmit} className='login-form'>
          <Input
            placeholder='Enter Name'
            name='name'
            required
            value={name}
            onChange={this.changeHandler}
          />
          <Input
            style={{ marginTop: '10px' }}
            placeholder='Enter Email'
            name='email'
            required
            value={email}
            onChange={this.changeHandler}
          />
          <Input.Password
            style={{ marginTop: '10px' }}
            placeholder='Enter password'
            name='password'
            required
            value={password}
            onChange={this.changeHandler}
          />
          <Select
            style={{ marginTop: '10px', width: '100%' }}
            onChange={this.selectHandler}
          >
            <Option value='teacher'>Teacher</Option>
            <Option value='student'>Student</Option>
          </Select>
          <p style={{ marginTop: '10px' }}>
            <button type='submit'>Sign Up</button>
          </p>
        </form>
        <a href='/login'>Login</a>
      </div>
    )
  }
}

export default SignUp
