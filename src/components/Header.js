import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
const Header = () => {
  const [isLoggedOut, setIsLoggedOut] = useState(false)
  const logout = () => {
    if (sessionStorage.getItem('userData')) {
      sessionStorage.setItem('userData', null)
      sessionStorage.clear()
      setIsLoggedOut(true)
    }
  }

  return (
    <div>
      {isLoggedOut ? <Redirect to={'/login'} /> : ''}
      <ul className='app-list'>
        <ol>
          <Link to='/dashboard'>Dashboard</Link>
        </ol>
        <ol>
          <button onClick={logout}>Logout</button>
        </ol>
      </ul>
    </div>
  )
}
export default Header
