import { Button, Input, Modal, Select } from 'antd'
import React from 'react'
import './index.css'
const { Option } = Select
const EditModal = props => {
  const { isOpen, handleModal, data } = props

  return (
    <Modal
      title={
        data && data.color ? `Edit word description` : 'Add word description'
      }
      visible={isOpen}
      onCancel={handleModal}
      footer={null}
    >
      <div className=''>
        <div>
          <label>
            <strong>Color:</strong>
          </label>
          <Select
            value={(data && data.color) || ''}
            placeholder='Select Color'
            style={{ marginTop: '10px', width: '100%' }}
            onChange={props.changeColor}
          >
            <Option value='blue'>Blue</Option>
            <Option value='green'>Green</Option>
            <Option value='red'>Red</Option>
          </Select>
        </div>
        <div style={{ marginTop: '8px' }}>
          <label>
            <strong>Description:</strong>
          </label>
          <Input
            placeholder='add some description'
            value={(data && data.description) || ''}
            name='description'
            onChange={props.changeHandler}
          />
        </div>
        <div style={{ marginTop: '8px' }}>
          <label>
            <strong>Upload Image:</strong>
          </label>
          <Input
            label='Add Item'
            placeholder=''
            value={(data && data.image) || ''}
            name='image'
            onChange={props.changeHandler}
          />
        </div>
        <div style={{ marginTop: '8px' }}>
          <label>
            <strong>Video url:</strong>
          </label>
          <Input
            label='Add Item'
            placeholder=''
            value={(data && data.video_url) || ''}
            name='video_url'
            onChange={props.changeHandler}
          />
        </div>

        <Button
          type='primary'
          style={{ marginTop: '10px' }}
          onClick={e => props.submitHandler(data)}
        >
          Update
        </Button>
      </div>
    </Modal>
  )
}
export default EditModal
