import React from 'react'
import './App.css'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import LoginForm from './components/LoginForm'
import Dashboard from './components/Dashboard'
import Add from './components/Add'
import SignUp from './components/SignUp'
import Edit from './components/Edit'

function App () {
  return (
    <div className='App'>
      <Router>
        <Switch>
          <Route exact path='/' component={LoginForm} />
          <Route exact path='/login' component={LoginForm} />
          <Route exact path='/dashboard' component={Dashboard} />
          <Route exact path='/add' component={Add} />
          <Route exact path='/signup' component={SignUp} />
          <Route exact path='/edit' component={Edit} />
        </Switch>
      </Router>
    </div>
  )
}

export default App
