import React, { useState } from 'react'
import { Button, Input, message } from 'antd'
import './index.css'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import Header from './Header'
const { TextArea } = Input
const Add = props => {
  const [title, setTitle] = useState('')
  const [desc, setDesc] = useState('')
  const [err, setErr] = useState('')
  if (!sessionStorage.getItem('userData')) {
    return <Redirect to={'/login'} />
  }
  const changeTitle = e => {
    if (e.target.name === 'title') {
      setTitle(e.target.value)
    } else {
      setDesc(e.target.value)
    }
  }
  const submit = () => {
    if (!title && !desc) {
      setErr('Please fill all the fields.')
      return false
    } else {
      let payload = {
        title: title,
        desc: desc
      }
      // pass payload in api
      const userData = JSON.parse(sessionStorage.getItem('userData'))
      axios
        .post(
          `http://localhost:4000/api/v1/lesson/user/${userData.id}`,
          payload
        )
        .then(res => {
          let data = res.data
          if (data.success) {
            message.success(data.msg)
            setTitle('')
            setDesc('')
          }
        })
        .catch(err => {
          message.error('DB error.')
        })
    }
  }

  return (
    <div className='container'>
      <Header />
      <h2>Add Lesson</h2>
      <div className='login-form'>
        <Input
          label='Add Item'
          placeholder='Enter title'
          value={title}
          name='title'
          onChange={changeTitle}
        />

        <TextArea
          style={{ marginTop: '10px' }}
          rows={4}
          value={desc}
          placeholder='Enter description'
          onChange={changeTitle}
        />
        <Button type='primary' style={{ marginTop: '10px' }} onClick={submit}>
          Add
        </Button>
      </div>
      <span style={{ color: 'red' }}>{err}</span>
    </div>
  )
}
export default Add
